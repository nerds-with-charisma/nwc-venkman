import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { fetchData } from '../components/component-system';

import About from '../components/HomePage/About';
// import Head from 'next/head';
import Hero from '../components/HomePage/Hero';
import PortfolioWrap from '../components/HomePage/PortfolioWrap';
import Services from '../components/HomePage/Services';
import { GetStaticPropsResult } from 'next';
import { getMediaQuery } from '../components/component-system';

const HomePage: React.FC = ({ data }: any) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: 'INITIAL_DATA',
      payload: data,
    });
  }, [data]);

  return (
    <section id="HomePage">
      {/* Head */}

      <Hero />

      <About />

      <PortfolioWrap />

      <Services />
    </section>
  );
};

export default HomePage;

interface Data {
  data: any;
}

interface Res {
  req: any;
}

export async function getServerSideProps(
  ctx: Res,
): Promise<GetStaticPropsResult<Data>> {
  const cmsData = await fetchData('homepage');
  const works = await fetchData('works');

  return {
    props: {
      data: {
        mq: getMediaQuery(ctx.req.headers) ? 'mobile' : 'desktop',
        ...cmsData,
        works,
      },
    },
  };
}
