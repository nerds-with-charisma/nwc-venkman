import Document, { Html, Head, Main, NextScript } from 'next/document';

import config from '../components/lib/config';

export default class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          {/* Google Analytics */}
          {config.gaId !== null && (
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${config.gaId}`}
            />
          )}

          {config.gaId !== null && (
            <script
              dangerouslySetInnerHTML={{
                __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${config.gaId}', {
                page_path: window.location.pathname,
              });`,
              }}
            />
          )}

          {/* Google Tag Manager  */}
          {config.gtmId !== null && (
            <script
              dangerouslySetInnerHTML={{
                __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','${config.gtmId}');`,
              }}
            />
          )}

          {/* Meta nodes */}
          {config.meta &&
            config.meta.length > 0 &&
            config.meta.map((item) => (
              <meta key={item.key} name={item.key} content={item.value} />
            ))}

          {/* Open Graph */}
          {config.og &&
            Object.keys(config.og).map((key, value) => (
              <meta
                property={`og:${key}`}
                content={config.og[key]}
                key={value}
              />
            ))}

          {/* Schema */}
          {config.schemaMarkup &&
            config.schemaMarkup.map((item) => (
              <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{
                  __html: JSON.stringify(item).replace(/&quot;/g, '"'),
                }}
              />
            ))}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
