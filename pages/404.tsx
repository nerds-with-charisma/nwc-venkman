import Image from 'next/image';
import Link from 'next/link';
import { GradientText } from '../components/component-system';

const FourOhFour = () => (
  <section id="FourOhFour" className="p-40 text-center">
    <div className="text-9xl font-bold">
      <GradientText text="404" />
    </div>
    <Image src="/images/404.gif" width={498} height={212} />
    <p className="text-gray-600 text-lg mt-10">
      {"Bummer. We couldn't find what you're looking for."}
    </p>
    <br />
    <Link href="/">
      <a>{'Go Home'}</a>
    </Link>
  </section>
);

export default FourOhFour;
