/* Delete ME */
const exampleHandler = (req, res) => {
  if (req.method === 'GET') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');

    res.send({
      test: 'Hi',
      cookieInfo: req.headers.cookie,
    });
  } else {
    res.send('Do Post stuff');
  }
};

export default exampleHandler;
