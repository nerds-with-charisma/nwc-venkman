import mailchimp from '@mailchimp/mailchimp_marketing';

mailchimp.setConfig({
  apiKey: process.env.NEXT_PUBLIC_MAIL_CHIMP_API_KEY,
  server: process.env.NEXT_PUBLIC_MAIL_CHIMP_SERVER,
});

export default async (req, res) => {
  const { email, message } = req.body;

  if (!email || !message) {
    return res.status(400).json({ error: 'Please fill out all fields' });
  }

  try {
    const response = await mailchimp.lists.addListMember(
      process.env.NEXT_PUBLIC_MAIL_CHIMP_LIST_ID,
      {
        email_address: email,
        status: 'subscribed',
        merge_fields: {
          FNAME: '',
          LNAME: '',
          MESSAGE: message,
        },
      },
    );

    console.log(`!!! ${response}`);

    return res.status(201).json({ error: false, message: 'THANKS!' });
  } catch (error) {
    console.log(`$$$ ${JSON.stringify(error.response.text, null, '\t')}`);
    return res.status(400).json({ error: JSON.parse(error.response.text) });
  }
};
