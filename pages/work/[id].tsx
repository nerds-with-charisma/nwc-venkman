import db from '../../components/lib/db-temp';

import Fade from 'react-reveal/Fade';

import BackButton from '../../components/Common/BackButton';
import Description from '../../components/Portfolio/Description';
import Photos from '../../components/Portfolio/Photos';
import PortfolioHeading from '../../components/Portfolio/PortfolioHeading';
import Technology from '../../components/Portfolio/Technology';

const Work: React.FC = ({ data }: any) => (
  <section id="Work">
    <PortfolioHeading d={data} />

    <Fade bottom delay={500}>
      <Technology d={data} />
    </Fade>
    <Fade bottom delay={500}>
      <Description d={data} />
    </Fade>
    <Fade bottom delay={500}>
      <Photos d={data} />
    </Fade>
    <BackButton />
  </section>
);

export default Work;

/* get all possible portfolio ids */
export function getAllIds() {
  return db.portfolio.projects.map((project) => {
    return {
      params: {
        id: project.slug,
      },
    };
  });
}

/* build all the pages via the paths received above */
export async function getStaticPaths() {
  const paths = getAllIds();
  return {
    paths,
    fallback: false,
  };
}

/* get the specific port data */
export function getProjectData(id) {
  // // Combine the data with the id
  const result = db.portfolio.projects.filter((obj) => {
    return obj.slug === id;
  });
  return {
    id,
    ...result[0],
  };
}

/* send the data to props */
export async function getStaticProps({ params }) {
  const data = getProjectData(params.id);
  return {
    props: {
      data,
    },
  };
}
