import { ApolloClient, InMemoryCache, gql } from '@apollo/client';

interface TestTypes {
  id: number;
  Title: string;
}

interface Props extends Array<TestTypes> {
  tests: TestTypes;
}

const ExampleGraphQl = ({ tests }: Props) => {
  // console.log(tests);

  return (
    <section className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
      <h1>GraphQl Example</h1>
      <br />
      {tests &&
        tests instanceof Array &&
        tests.map((test) => (
          <div key={test.id}>
            {test.id} :: {test.Title}
          </div>
        ))}
    </section>
  );
};

export default ExampleGraphQl;

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: 'http://localhost:1337/graphql',
    cache: new InMemoryCache(),
  });

  const { data } = await client.query({
    query: gql`
      query GetTests {
        tests {
          Title
          id
        }
      }
    `,
  });

  return {
    props: {
      tests: data.tests,
    },
  };
}
