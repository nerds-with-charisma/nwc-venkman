import Image from 'next/image';

interface DataObject {
  mainImage: string;
}
interface Props {
  d: DataObject;
}

const Photos: React.FC<Props> = ({ d }) => {
  if (!d) return <div id="Photos-loading" />;

  return (
    <section id="Photos" className="text-center -mt-40">
      <Image src={d.mainImage} width={1248} height={917} />
    </section>
  );
};

export default Photos;
