import { GradientText } from '../component-system';

interface DataObject {
  bg: string;
  description: string;
  stack: string;
}
interface Props {
  d: DataObject;
}

const Description: React.FC<Props> = ({ d }) => {
  if (!d) return <div id="Description-loading" />;

  return (
    <section
      id="Description"
      style={{
        backgroundImage: `url(${d.bg})`,
        backgroundPosition: 'center right',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}
    >
      <div className="container mx-auto pt-32 pb-32 mt-20 mb-20 text-center">
        <div className="text-5xl font-bold">
          <GradientText text="What We Did" from="#F3FE69" to="#FFAD3C" />
        </div>
        <br />
        <p className="text-lg text-white leading-loose">
          {d.description}
          <br />
          <br />
          {d.stack}
        </p>
      </div>
    </section>
  );
};

export default Description;
