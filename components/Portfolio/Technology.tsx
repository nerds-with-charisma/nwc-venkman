import Image from 'next/image';

const Technology = ({ d }) => {
  if (!d) return <div id="Technology-loading" />;
  return (
    <section id="Technology" className="container mx-auto pt-32">
      <div className="md:flex">
        <div className="md:flex-1 text-right pr-10">
          <div className="text-2xl pt-8">
            {d.tech.map((t) => (
              <h2
                key={t}
                className="text-3xl md:text-8xl font-bold leading-none"
              >
                {t}
              </h2>
            ))}
            <div className="text-3xl text-gray-400">{`Launched ${d.launched}`}</div>
          </div>
        </div>

        <div className="md:flex-1 text-left align-middle inline-block">
          <br />
          <span className="shadow-lg">
            <Image width={542} height={354} src={d.thumbnail} />
          </span>
        </div>
      </div>
    </section>
  );
};

export default Technology;
