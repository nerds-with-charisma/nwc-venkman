import Image from 'next/image';
import Fade from 'react-reveal/Fade';
import { useSelector } from 'react-redux';

import { GradientText } from '../component-system';

interface DataObject {
  bg: string;
  deviceImage: string;
  gradientFrom: string;
  gradientTo: string;
  title: string;
}
interface Props {
  d: DataObject;
}

const PortfolioHeading: React.FC<Props> = ({ d }) => {
  const mq = useSelector((state) => state.mq);

  if (!d) return <div id="Hero-loading" />;

  return (
    <section
      id="Hero"
      className="relative text-center pt-60 leading-none text-7xl font-bold h-screen z-20"
      style={{
        backgroundImage: `url(${d.bg})`,
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        minHeight: 400,
      }}
    >
      <Fade top delay={500}>
        <div>
          <GradientText
            from={d.gradientFrom}
            to={d.gradientTo}
            text={d.title}
          />
        </div>
      </Fade>

      <div
        className={
          mq === 'mobile'
            ? 'absolute w-screen -bottom-12'
            : 'absolute -bottom-20 w-screen'
        }
      >
        <Fade bottom delay={500}>
          <Image
            className="pt-3"
            src={d.deviceImage}
            alt={d.title}
            width={mq === 'mobile' ? 206 : 413}
            height={mq === 'mobile' ? 389 : 778}
          />
        </Fade>
      </div>
    </section>
  );
};

export default PortfolioHeading;
