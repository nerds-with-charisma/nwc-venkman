import Link from 'next/link';
import { GradientText } from '../component-system';

const BackButton = () => (
  <section id="BackButton" className="text-center">
    <br />
    <br />
    <Link href="/#PortfolioWrap">
      <a className="text-xl border border-gray-200 pt-3 pb-3 pr-10 pl-10 rounded-full shaodw-sm hover:shadow-md transition-all">
        <GradientText text="Back to Portfolio" />
      </a>
    </Link>
  </section>
);

export default BackButton;
