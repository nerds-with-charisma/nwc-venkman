import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';

const NavLinks: React.FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const navigation = useSelector((state) => state.navigation);

  const navFunc = (item) => {
    if (item.slug === 'contact') {
      router.push('#contact', undefined, { shallow: true });
      dispatch({
        type: 'TOGGLE_CONTACT',
        payload: true,
      });
    } else {
      console.log('zuuldo: Scroll to anchor');
      router.push(item.url);
    }
  };

  return (
    <section id="NavLinks" className="mt-20">
      {navigation.navigation.map((item) => (
        <div
          key={item.slug}
          className="text-white text-5xl mb-12 hover:text-nwcBlue"
        >
          <button
            className="w-screen focus:outline-white"
            type="button"
            onClick={() => navFunc(item)}
          >
            <strong>{item.title}</strong>
          </button>
        </div>
      ))}
    </section>
  );
};

export default NavLinks;
