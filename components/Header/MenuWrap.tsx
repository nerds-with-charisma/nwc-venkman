import { useDispatch, useSelector } from 'react-redux';

import { Mask, NavigationButton } from '../component-system';

import NavLinks from './NavLinks';

const MenuWrap: React.FC = () => {
  const dispatch = useDispatch();
  const navOpen = useSelector((state) => state.navOpen);

  const toggleNav = () => {
    dispatch({
      type: 'TOGGLE_NAV',
      payload: !navOpen,
    });
  };

  return (
    <section id="MenuWrap">
      <NavigationButton
        callback={() => toggleNav()}
        color="#fff"
        icon={navOpen === false ? 'asc' : 'close'}
        theme="focus:outline-white hover:opacity-50 transition-all"
      />

      <Mask
        callback={() => toggleNav()}
        color="rgb(0,0,0,0.8)"
        gradient="bg-gradient-to-r from-purple-900 to-blue-700"
        visible={navOpen}
      >
        <NavLinks />
      </Mask>
    </section>
  );
};

export default MenuWrap;
