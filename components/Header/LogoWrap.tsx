import Image from 'next/image';
import Link from 'next/link';

const LogoWrap = () => (
  <section id="LogoWrap" className="text-right pt-3">
    <Link href="/">
      <a className="transition-all hover:opacity-50">
        <Image
          src="/images/logo.svg"
          alt="Nerds With Charisma"
          width={45}
          height={22}
        />
      </a>
    </Link>
  </section>
);

export default LogoWrap;
