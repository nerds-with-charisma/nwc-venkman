import LogoWrap from './LogoWrap';
import MenuWrap from './MenuWrap';

const HeaderWrap = () => (
  <header id="HeaderWrap" className="fixed w-full z-50">
    <section className="container mx-auto pt-12 grid grid-cols-2">
      <section>
        <MenuWrap />
      </section>
      <section>
        <LogoWrap />
      </section>
    </section>
  </header>
);

export default HeaderWrap;
