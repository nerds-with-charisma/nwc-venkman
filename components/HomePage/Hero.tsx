import Image from 'next/image';
import Fade from 'react-reveal/Fade';
import { useSelector } from 'react-redux';

import { GradientText } from '../component-system';

const Hero: React.FC = () => {
  const hero = useSelector((state) => state.hero);
  const mq = useSelector((state) => state.mq);

  if (!hero) return false;
  return (
    <section
      id="Hero"
      className="relative text-center pt-32 leading-none text-6xl md:text-big font-bold h-screen z-20"
      style={{
        backgroundImage: `url(${hero.bg})`,
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        minHeight: 400,
      }}
    >
      <Fade top delay={500}>
        <div>
          <GradientText
            from={hero.tagline[0].from}
            to={hero.tagline[0].to}
            text={hero.tagline[0].title}
          />
          <br />
          <GradientText
            from={hero.tagline[1].from}
            to={hero.tagline[1].to}
            text={hero.tagline[1].title}
          />
          <br />
          <GradientText
            from={hero.tagline[2].from}
            to={hero.tagline[2].to}
            text={hero.tagline[2].title}
          />
        </div>
      </Fade>

      <div
        className={
          mq === 'mobile'
            ? 'absolute w-screen -bottom-12'
            : 'absolute -bottom-36 w-screen'
        }
      >
        <Fade bottom delay={500}>
          <Image
            className="pt-3"
            src={hero.image}
            alt={hero.alt}
            width={mq === 'mobile' ? 206 : 413}
            height={mq === 'mobile' ? 389 : 778}
          />
        </Fade>
      </div>
    </section>
  );
};

export default Hero;
