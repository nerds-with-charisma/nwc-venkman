import Image from 'next/image';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import Fade from 'react-reveal/Fade';

const PortfolioWrap: React.FC = () => {
  const portfolio = useSelector((state) => state.portfolio);
  const works = useSelector((state) => state.works);

  const bgs = [
    'bg-nwcBlue',
    'bg-nwcPink',
    'bg-nwcPurple',
    'bg-nwcGreen',
    'bg-nwcYellow',
    'bg-nwcOrange',
  ];

  if (!portfolio || !works || works.length < 1) return false;

  return (
    <section
      className="mx-auto pt-32 grid grid-cols-2 md:grid-cols-3 gap-2"
      id="PortfolioWrap"
    >
      {works &&
        works.map((p, i) => (
          <div key={i} className={bgs[Math.floor(Math.random() * bgs.length)]}>
            <Link href={`/work/${p.slug}`}>
              <a>
                <Image
                  src={p.thumbnail}
                  height={354}
                  width={619}
                  layout="responsive"
                  className="transition-all hover:opacity-50"
                />
              </a>
            </Link>
          </div>
        ))}
    </section>
  );
};

export default PortfolioWrap;
