import { useEffect, useState } from 'react';
import Image from 'next/image';
import { useSelector } from 'react-redux';
import Fade from 'react-reveal/Fade';

import { GradientText, useIsInView } from '../component-system';

const About: React.FC = () => {
  const about = useSelector((state) => state.about);
  const [fadeIn, fadeInSetter] = useState(false);

  useEffect(() => {
    document.addEventListener('scroll', setterFunc);
  }, []);

  const setterFunc = () => {
    if (useIsInView('About') === true) fadeInSetter(true);
  };

  if (!about) return false;

  return (
    <section
      id="About"
      className="container mx-auto pt-32"
      style={{
        backgroundImage: `url(${about.bg})`,
        backgroundPosition: 'center right',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
      }}
    >
      <div className="md:flex">
        <Fade left when={fadeIn === true}>
          <div className="md:flex-1 text-right pr-10">
            {about &&
              about.tagline.map((v) => (
                <h2
                  key={v}
                  className="text-3xl md:text-8xl font-bold leading-none"
                >
                  {v}
                </h2>
              ))}

            <div className="text-2xl">
              <GradientText
                text={
                  about.randomQuote[
                    Math.floor(Math.random() * about.randomQuote.length)
                  ]
                }
              />
            </div>
          </div>
        </Fade>
        <Fade right when={fadeIn === true}>
          <div className="md:flex-1 text-center align-middle	inline-block">
            <br />
            <Image width={518} height={370} src={about.image} />
          </div>
        </Fade>
      </div>
    </section>
  );
};

export default About;
