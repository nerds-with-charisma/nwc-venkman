// theme
interface Props {
  /**
   * callback function after being clicked
   */
  callback?: () => void;
  children: JSX.Element;
  /**
   * color of the mask, in rgb format
   */
  color?: string;
  /**
   * if supplied, it will overwrite the background color
   */
  gradient?: string;
  /**
   * how much you can see through the mask, the lower the less visible the mask will be
   */
  opacity?: number;
  /**
   * custom classes to be applied to the parent wrapper
   */
  theme?: string;
  /**
   * should the mask be shown or not. You can also use an external state variable and default this to true
   */
  visible?: boolean;
}

const Mask = ({
  callback,
  children,
  color = 'rgb(36,25,38)',
  gradient = null,
  opacity = 90,
  theme = 'text-center p-4 text-white',
  visible = false,
}: Props): JSX.Element => {
  const maskStyles = {
    background: !gradient ? color : null,
  };

  if (visible !== true) return null;

  return (
    <section
      id="Mask"
      data-text="mask"
      className={`nwc--mask fixed inset-0 h-screen z-40 ${theme}`}
    >
      <div className="relative z-50">{children}</div>
      <div
        onClick={callback}
        className={`nwc--mask fixed inset-0 h-screen z-40 ${gradient} opacity-${opacity}`}
        style={maskStyles}
      />
    </section>
  );
};

export default Mask;
