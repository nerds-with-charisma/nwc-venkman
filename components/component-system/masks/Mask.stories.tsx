import React from 'react';
import Mask from './Mask';

export default {
  title: 'Masks/Mask',
  component: Mask,
};

const Template = (args) => (
  <div>
    <h1>Content</h1>
    <Mask {...args}>{args.children}</Mask>
  </div>
);

const defaultProps = {
  children: <h1>Hello There</h1>,
  visible: true,
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};

export const Callback = Template.bind({});
Callback.args = {
  ...defaultProps,
  callback: () => console.log('clicked the mask'),
};

export const Color = Template.bind({});
Color.args = {
  ...defaultProps,
  color: 'rgb(121,23,255)',
};

export const Gradient = Template.bind({});
Gradient.args = {
  ...defaultProps,
  gradient: 'bg-gradient-to-r from-purple-400 via-pink-500 to-red-500',
};

export const Opacity = Template.bind({});
Opacity.args = {
  ...defaultProps,
  opacity: '30',
};

export const Theme = Template.bind({});
Theme.args = {
  ...defaultProps,
  theme: 'p-20 text-red-500 text-right',
};
