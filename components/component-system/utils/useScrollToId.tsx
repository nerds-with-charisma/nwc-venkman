const useScrollToId = (id: string): boolean => {
  const el = document.getElementById(id);
  if (el) {
    el.scrollIntoView();
    return true;
  } else {
    return false;
  }
};

export default useScrollToId;
