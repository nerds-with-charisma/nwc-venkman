import React from 'react';
import GradientText from './GradientText';

export default {
  title: 'Typography/Gradient Text',
  component: GradientText,
};

const Template = (args) => <GradientText {...args} />;

const defaultProps = {};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};
