interface Props {
  from?: string;
  text: string;
  to?: string;
}

const GradientText: React.FC<Props> = ({
  from = '#9300ff',
  to = '#00ebff',
  text = 'Hello There',
}) => {
  const TextStyles = {
    background: `linear-gradient(35deg, ${from}, ${to})`,
    '-webkit-background-clip': 'text',
    '-webkit-text-fill-color': 'transparent',
  };

  return <span style={TextStyles}>{text}</span>;
};

export default GradientText;
