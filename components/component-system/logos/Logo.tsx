interface Props {
  alt?: string;
  src: string;
  width?: number;
}

const Logo: React.FC<Props> = ({ alt, src, width }) => (
  <section id="Log">{src && <img src={src} alt={alt} width={width} />}</section>
);

export default Logo;
