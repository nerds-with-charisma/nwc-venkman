import React from 'react';
import NavigationButton from './NavigationButton';

export default {
  title: 'Navigation/Button',
  component: NavigationButton,
};

const Template = (args) => <NavigationButton {...args} />;

const defaultProps = {
  callback: () => console.log('clicked'),
  title: 'navigation',
  parameters: {
    notes: '',
  },
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};

export const Color = Template.bind({});
Color.args = {
  ...defaultProps,
  color: '#733791',
};

export const Icon = Template.bind({});
Icon.args = {
  ...defaultProps,
  icon: 'desc',
};

export const Size = Template.bind({});
Size.args = {
  ...defaultProps,
  size: 24,
};

export const Theme = Template.bind({});
Theme.args = {
  ...defaultProps,
  theme:
    'border-2 border-indigo-600 text-indigo-600 hover:text-gray-500 hover:border-gray-500',
};

export const Title = Template.bind({});
Title.args = {
  ...defaultProps,
  title: 'accessibility title',
};
