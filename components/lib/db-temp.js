const db = {
  about: {
    bg: '/images/about--bg@2x.jpg',
    image: '/images/about--logo@2x.png',
    tagline: ['websites.', 'apps.', 'seo.', 'better.'],
    randomQuote: [
      'Coding Since Before We Could Walk.',
      'The Best in the World at What We Do.',
      'Super-Awesome Apps for Super-Awesome Peeps.',
      "I Think We're Gonna Need A Bigger Boat.",
      'Coding Ninjas.',
      'Punk Rock Pixels.',
      'The Best There Is, Was, and Ever Will Be.',
      'Do Or Do Not. There Is No Try.',
      'We Know "The Google".',
      'FOR SCIENCE!',
      'Reinforced by Space-Age Technology.',
    ],
  },
  footer: {
    dnd: 'Designed & Developed By',
    copyright: `copyright © ${new Date().getFullYear()} nerds with charisma`,
    contactInfo: JSON.stringify(
      `<div>
        <a href="/docs/brian-dausman-resume-2019.pdf">
          <h2 class="text-nwcPurple inline">brian dausman</h2>
        </a> +
        <a href="http://www.andrewbieganski.com/images/andrewbieganski2018.pdf" target="_blank" rel="noopener noreferrer">
          <h2 class="text-nwcPurple inline">
            andrew bieganski
          </h2>
        </a>
        <br />
        <br />
      </div>`,
    ),
    social: [
      {
        url: 'https://gitlab.com/nerds-with-charisma',
        title: 'GitLab',
        icon: '/images/logos/logo--gitlab.svg',
      },
      {
        url: 'https://twitter.com/nerdswcharisma',
        title: 'Twitter',
        icon: '/images/logos/logo--twitter.svg',
      },
      {
        url: 'https://www.npmjs.com/~nerds-with-charisma',
        title: 'NPM',
        icon: '/images/logos/logo--npm.svg',
      },
      {
        url: 'https://medium.com/@nerdswithcharisma',
        title: 'Medium',
        icon: '/images/logos/logo--medium.svg',
      },
      {
        url: 'https://jamstack-jedi.nerdswithcharisma.com/',
        title: 'Jamstack Jedi',
        icon: '/images/logos/logo--jedi.svg',
      },
    ],
    tagline: "NWC making bitchin' websites since the 90s!",
  },
  hero: {
    alt: 'NWC',
    bg: '/images/hero--bg@2x.jpg',
    image: '/images/hero--phone@2x.png',
    tagline: [
      {
        title: 'NERDS',
        from: '#9300FF',
        to: '#1FECFD',
      },
      {
        title: 'WITH',
        from: '#F3FE69',
        to: '#00EBFF',
      },
      {
        title: 'CHARISMA',
        from: '#FFEA4A',
        to: '#FFAD3C',
      },
    ],
  },
  navigation: [
    {
      slug: 'home',
      url: '/',
      title: 'Home',
      isActive: true,
    },
    {
      slug: 'portfolio',
      url: '/portfolio',
      title: 'Portfolio',
      isActive: false,
    },
    {
      slug: 'contact',
      url: '/contact',
      title: 'Contact',
      isActive: false,
    },
  ],
  portfolio: {
    bg: '/images/hero--bg@2x.jpg',
    gradientFrom: '#FFEA4A',
    gradientTo: '#FFAD3C',
    projects: [
      {
        bg: '/images/hero--bg@2x.jpg',
        mainImage: '/images/portfolio/portfolio--upendo@2x.png',
        slug: 'upendo-staffing',
        thumbnail: '/images/portfolio/browser--upendo@2x.jpg',
        title: 'Upendo Staffing',
        deviceImage: '/images/hero--phone@2x.png',
        tech: ['Web Dev.', 'Jamstack.', 'Design & UX.'],
        launched: '2020',
        description:
          'Upendo Staffing is a recruiting firm based in Florida that previously had a simple, static Wix website. We gave them a jolt & hooked them up with a SUPER-sweet Jamstack site that allows them full control of their content as well as a job board to attract potential recruits and improve their workflow!',
        stack: '[Gatsby, React, GraphQl, Adobe Xd]',
      },
      {
        slug: 'daily-fit-blend',
        thumbnail: '/images/portfolio/thumb--daily-fit-blend.jpg',
        title: 'Daily Fit Blend',
      },
      {
        slug: 'nerd-fit',
        thumbnail: '/images/portfolio/thumb--nerd-fit.jpg',
        title: 'Nerd Fit',
      },
      {
        slug: 'sears-outlet',
        thumbnail: '/images/portfolio/thumb--sears-outlet.jpg',
        title: 'Sears Outlet',
      },
      {
        slug: 'chamberlain-staffing',
        thumbnail: '/images/portfolio/thumb--chamberlain.jpg',
        title: 'Chamberlain Staffing',
      },
      {
        slug: 'axiom-tech-group',
        thumbnail: '/images/portfolio/thumb--axiom.jpg',
        title: 'Axiom Tech Group',
      },
      {
        slug: 'camp-play-a-lot',
        thumbnail: '/images/portfolio/thumb--camp-play-alot.jpg',
        title: 'Camp Play A Lot',
      },
      {
        slug: 'tpsr-alliance',
        thumbnail: '/images/portfolio/thumb--tpsr.jpg',
        title: 'TPSR Alliance',
      },
      {
        slug: 'premier-academy',
        thumbnail: '/images/portfolio/thumb--premier-academy.jpg',
        title: 'Premier Academy',
      },
      {
        slug: 'ladse',
        thumbnail: '/images/portfolio/thumb--ladse.jpg',
        title: 'LADSE',
      },
      {
        slug: 'ob-chamber',
        thumbnail: '/images/portfolio/thumb--ob-chamber.jpg',
        title: 'Oak Brook Chamber of Commerce',
      },
      {
        slug: 'sos-illinois',
        thumbnail: '/images/portfolio/thumb--sos-illinois.jpg',
        title: 'SOS Illinois',
      },
      {
        slug: 'Stericycle',
        thumbnail: '/images/portfolio/thumb--stericycle.jpg',
        title: 'Stericycle',
      },
      {
        slug: 'bfg-tech',
        thumbnail: '/images/portfolio/thumb--bfg.jpg',
        title: 'BFG Tech',
      },
      {
        slug: 'niu-outreach',
        thumbnail: '/images/portfolio/thumb--niu-outreach.jpg',
        title: 'NIU Outreach',
      },
    ],
  },
  services: {
    bg: '/images/services--bg@2x.jpg',
    tagline1: 'We Can Help',
    tagline2: 'Build Your Brand',
    copy:
      "Nerds With Charisma are a small band of designers, developers, & creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it. We can bring fresh ideas and a new approach to your brand. We don't just build your website, we build and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life. We’re also very comfortable working with people pretty much anywhere in the world. We utilize the latest technologies & trends to get your site setup and rocking. Our help doesn’t stop at just a website. Sure, we can help design & develop your site, but we will also discuss and explain your user’s experience, come up with a content strategy for continued success, get you going on social media, explain the new technologies, go over analytics, and get your SEO juice flowing. We also really enjoy doing websites for nonprofits. We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we’ll let you know what we can do.",
    logos: '/images/services--logos.svg',
    listBg: '/images/services--color@2x.jpg',
    development: [
      'NextJs / Gatsby',
      'JamStack',
      'Mobile Apps / React Native',
      'Wordpress Websites',
      'SEO & Marketing',
      'Analytics, Tagging, Tracking Pixels',
      'Backups & Monitoring',
      'eCommerce',
    ],
    design: [
      'Business Cards',
      'Logos & Branding',
      'Stationary',
      'E-Blasts',
      'Social & Strategy',
      'Keyword Research',
      'Analytics',
      'And More..',
    ],
  },
};

export default db;
