import config from './config';

export const pageview = (url: URL) => {
  if (config.gaId === null) return false;

  window.gtag('config', config.gaId, {
    page_path: url,
  });
};

type GTagEvent = {
  action: string;
  category: string;
  label: string;
  value: number;
};

export const event = ({ action, category, label, value }: GTagEvent) => {
  if (config.gaId === null) return false;

  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value,
  });
};
