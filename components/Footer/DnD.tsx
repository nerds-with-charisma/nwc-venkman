import { useSelector } from 'react-redux';

const DnD = () => {
  const footer = useSelector((state) => state.footer);

  if (!footer) return false;

  return (
    <section id="DnD">
      <strong className="text-gray-900">{footer.dnd}</strong>
      <div
        dangerouslySetInnerHTML={{
          __html: JSON.parse(JSON.stringify(footer.contactInfo)),
        }}
      />
    </section>
  );
};

export default DnD;
