import { useSelector } from 'react-redux';

const Copyright = () => {
  const footer = useSelector((state) => state.footer);

  if (!footer) return false;
  return (
    <section id="Copyright" className="text-gray-600 text-sm p-4 leading-loose">
      {new Date().getFullYear()} {footer.copyright}
      <br />
      {footer.tagline}
    </section>
  );
};

export default Copyright;
