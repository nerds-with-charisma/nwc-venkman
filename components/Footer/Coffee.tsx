const Coffee = () => (
  <section id="Coffee">
    <a
      className="bmc-button"
      target="_blank"
      rel="noopener noreferrer"
      href="https://www.buymeacoffee.com/yz6MAXbTE"
    >
      <img
        style={bmcButton}
        src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg"
        alt="Buy me a coffee"
      />
      <span className="font--14"> Buy me a coffee</span>
    </a>
  </section>
);

export default Coffee;

/* styles */
const bmcButton = {
  width: 27,
  marginBottom: 1,
  boxShadow: 'none',
  border: 'none',
  verticalAlign: 'middle',
};

// .bmc-button {
//     line-height: 36px!important;
//     height: 37px!important;
//     display: inline-flex!important;
//     color: #000!important;
//     background-color: #fff!important;
//     border-radius: 3px!important;
//     border: 1px solid transparent!important;
//     padding: 1px 9px!important;
//     font-size: 22px!important;
//     letter-spacing: .6px!important;
//     box-shadow: 0 1px 2px hsla(0,0%,74.5%,.5)!important;
//     margin: 0 auto!important;
//     box-sizing: border-box!important;
//     -webkit-transition: all .3s linear!important;
//     transition: all .3s linear!important;
//     font-weight: 700
// }

// .bmc-button,.bmc-button:active,.bmc-button:focus,.bmc-button:hover {
//     text-decoration: none!important;
//     -webkit-box-shadow: 0 1px 2px 2px hsla(0,0%,74.5%,.5)!important
// }

// .bmc-button:active,.bmc-button:focus,.bmc-button:hover {
//     box-shadow: 0 1px 2px 2px hsla(0,0%,74.5%,.5)!important;
//     opacity: .85!important;
//     color: #fff!important;
//     background: #9012fe!important
// }
